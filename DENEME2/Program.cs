﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DENEME2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Customer customer1 = new Customer {Id=12,Firstname="Samet",Lastname="Dora",City="Sivas"};
            Customer customer2 = new Customer(13,"Merve","Güven","Sivas");
            Customer customer3 = new Customer();
            customer3.Id = 14;
            customer3.Firstname ="Metehan";
            customer3.Lastname = "Dora";
            customer3.City = "Ankara";

            Console.WriteLine(customer2.Firstname);
            Console.ReadKey();
        }
    }
    public class Customer
    {
        public Customer()
        {
            
        }

        public Customer(int id,string firstname,string lastname, string city)
        {
            Id = id;
            Firstname = firstname;
            Lastname = lastname;
            City = city;
        }


        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string City { get; set; }

    }
}
